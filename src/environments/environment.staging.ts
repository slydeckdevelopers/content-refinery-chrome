/**
 * This file defines the variables needed for the application to work
 * in *staging*
 */
import 'zone.js/dist/zone-error'; // Included with Angular CLI.
import configurationInterface from './configurationInterface';

const envconf: configurationInterface = {
  production: true,
  baseCookieDomain: 'd1zq6rt1tftnfx.cloudfront.net',
  //StripeClient: Stripe('pk_test_hjsPjuvA00y20tdlUR9CHbaY00ahRv6Pef'),

  AwsApiId: 'zy8crzd7j6',
  AwsRegion: 'us-west-2',
  CognitoClientId: '24e8e5j26gpj85hml7finmb5rb',
  CognitoIdentityPoolId: 'us-west-2:8229df9d-64a3-47dd-bbae-495bb8aa1e8b',
  CognitoUserPoolId: 'us-west-2_KboUYMC1L',
  OriginalFilesBucketName: 'cr-staging-stack-originalfilesbucket-1rya91dq12i0k',
  StageName: 'Staging',
  UnauthWebsocketApiId: '1vnhkbqjm4',
  UserFilesBucketName: 'cr-staging-stack-userfilesbucket-5n2xj206tazq'
};

export const environment: configurationInterface = {
  BaseApiUrl: `https://${envconf.AwsApiId}.execute-api.${envconf.AwsRegion}.amazonaws.com/${envconf.StageName}/`,
  UnauthWebSocketURL: `wss://${envconf.UnauthWebsocketApiId}.execute-api.${envconf.AwsRegion}.amazonaws.com/${envconf.StageName}`,
  CognitoLoginProviderURL: `cognito-idp.${envconf.AwsRegion}.amazonaws.com/${envconf.CognitoUserPoolId}`,
  ...envconf
};
