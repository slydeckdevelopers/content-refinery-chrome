/**
 * This file defines the variables needed for the application to work
 * in *production*
 */
import configurationInterface from './configurationInterface';

const envconf: configurationInterface = {
  production: true,
  baseCookieDomain: 'contentrefinery.io',
  //StripeClient: Stripe('pk_live_y2fIAEP86DorjK7Q9TxoqDkD009uIv41EC'),

  AwsApiId: '0hvxwu7md9',
  AwsRegion: 'us-west-2',
  CognitoClientId: 'qlqmlrs44t70sfq5mlsb5ddcj',
  CognitoIdentityPoolId: 'us-west-2:87b5f307-5a93-4d1b-a8ec-337f19a18920',
  CognitoUserPoolId: 'us-west-2_B2meSUZBe',
  OriginalFilesBucketName: 'cr-production-stack-originalfilesbucket-ctb4cqt5fco2',
  StageName: 'Production',
  UnauthWebsocketApiId: 'jgd1ktxatl',
  UserFilesBucketName: 'cr-production-stack-userfilesbucket-ssrnkes5wacd'
};

export const environment: configurationInterface = {
  BaseApiUrl: `https://${envconf.AwsApiId}.execute-api.${envconf.AwsRegion}.amazonaws.com/${envconf.StageName}/`,
  UnauthWebSocketURL: `wss://${envconf.UnauthWebsocketApiId}.execute-api.${envconf.AwsRegion}.amazonaws.com/${envconf.StageName}`,
  CognitoLoginProviderURL: `cognito-idp.${envconf.AwsRegion}.amazonaws.com/${envconf.CognitoUserPoolId}`,
  ...envconf
};
