import { Component, OnInit } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FilesService } from 'src/app/services/files.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-script',
  templateUrl: './script.component.html',
  styleUrls: ['./script.component.scss']
})
export class ScriptComponent implements OnInit {
  newUrlToken: string;
  newFileName: string;
  currentDomain: string = 'https://app.contentrefinery.io';

  constructor(
    private userService: UserService,
    private scriptToast: ToastrManager,
    private filesService: FilesService
  ) {}

  ngOnInit() {
    this.currentDomain = this.userService.currentDomain;
    this.buildScript();
  }

  buildScript() {
    this.filesService
      .getSelectedFile()
      .then((res) => {
        this.newUrlToken = res.Token;
        this.newFileName = res.Name;
      })
      .catch((error) => {
        this.scriptToast.errorToastr("Something went wrong! Please try again.");
      });
  }

  copyScript() {
    let code = document.getElementById('integrationScript').innerText;
    navigator.clipboard.writeText(code).then(()=>{
      this.scriptToast.successToastr('The Script was copied to clipboard!');
      this.closeModal();
    }).catch(()=>{
      this.scriptToast.errorToastr("There was an error while copying the script!");
      this.closeModal();
    });
  }
  
  closeModal() {

  }

}
