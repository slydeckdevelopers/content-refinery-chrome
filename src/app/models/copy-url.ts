export class CopyUrl {
  public FileToken: string;
  public ReceiverEmail: string;
  public ReceiverAlias: string;

  constructor() {
    // return a file settings instance with
    // the default value
    this.FileToken = '';
    this.ReceiverEmail = '';
    this.ReceiverAlias = '';
  }
}
